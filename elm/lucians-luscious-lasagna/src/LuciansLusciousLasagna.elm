module LuciansLusciousLasagna exposing (elapsedTimeInMinutes, expectedMinutesInOven, preparationTimeInMinutes)

expectedMinutesInOven = 40

preparationTimeInMinutes layersCount =
    layersCount * 2

elapsedTimeInMinutes layersCount spentMinutes =
  spentMinutes + preparationTimeInMinutes(layersCount)
