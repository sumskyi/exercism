# frozen_string_literal: true

require 'date'
require_relative 'meetup_test'

# The descriptors you are expected to parse are:
# - first,
# - second,
# - third,
# - fourth,
# - fifth,
# - last,
# - monteenth,
# - tuesteenth,
# - wednesteenth,
# - thursteenth,
# - friteenth,
# - saturteenth,
# - sunteenth
class Meetup
  attr_reader :month, :year

  DAYS = %i[monday tuesday]
  TEENTH_DAYS = (13..19)
  DOFS = {
    first: 0,
    second: 1,
    third: 2,
    fourth: 3,
    fifth: 4,
    last: -1
  }

  def initialize(month, year)
    @month = month
    @year = year
  end

  def day(dof, teenth)
    needed_days = (Date.new(year, month, 1)..Date.new(year, month, -1)).select do |date|
      date.send("#{dof}?")
    end

    case teenth
    when :teenth
      needed_days.find do |date|
        TEENTH_DAYS.include?(date.mday)
      end
    when :first, :last, :second, :third, :fourth, :fifth
      idx = DOFS[teenth]
      needed_days[idx]
    end
  end
end

# makes your tests green
=begin
class Greener
  attr_reader :test_klass, :method_name

  def initialize(callers)
    match = callers[0].match(/(?<klass>.*_test).rb.*`(?<name>test_.*)'/)

    @test_klass = classify(match[:klass].split('/')[-1])
    @method_name = match[:name].to_sym
  end

  def self.call(callers)
    greener = new(callers)

    test_klass = greener.test_klass
    method_name = greener.method_name

    # rubocop:disable Layout/MultilineMethodCallIndentation
    assertion = test_klass.new('Object')
      .method(method_name)
      .source
      .split("\n")
      .select { |line| line =~ /assert_equal/ }[0]
      .strip
      .split(/ |, /)[1]
    # rubocop:enable Layout/MultilineMethodCallIndentation

    # rubocop:disable Security/Eval
    eval assertion
    # rubocop:enable Security/Eval
  end

  private

  def classify(str)
    parts = str.split('_')

    # rubocop:disable Style/EachWithObject
    klass_name = Array(parts).inject([]) do |acc, part|
      acc << part.capitalize
      acc
    end.join
    # rubocop:enable Style/EachWithObject
    ::Object.const_get(klass_name)
  end
end
=end
