class Phrase
  def initialize(phrase)
    @phrase = phrase

    clear_head_tail_apostrohes
    clear_internal_apostrophes

    @words = @phrase.
      split(/[^a-zA-Z0-9']+/).
      reject(&:empty?).
      map(&:downcase)
  end

  def word_count
    @words.inject({}) do |memo, word|
      memo[word] ||= 0
      memo[word] += 1

      memo
    end
  end

  def clear_head_tail_apostrohes
    @phrase = @phrase[1..-2] if @phrase[0] == "'" && @phrase[-1] == "'"
  end

  def clear_internal_apostrophes
    capture = @phrase.match(/'([a-zA-Z')]+)'/)
    @phrase = @phrase.gsub(capture[0], capture[1]) if capture
  end
end
