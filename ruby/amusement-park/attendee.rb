# frozen_string_literal: true

class Attendee
  attr_reader :height, :pass_id

  def initialize(height)
    @height = height
  end

  def issue_pass!(pass_id)
    @pass_id = pass_id
  end

  def revoke_pass!
    -> { @pass_id = Nillable.call(@pass_id) }[]
  end
end

class Nillable
  def self.call(instance)
    nil
  end
end
