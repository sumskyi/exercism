# frozen_string_literal: true

require 'matrix'

MyMatrix = Matrix.dup

class Matrix
  def initialize(str)
    arr = str.split("\n").map{ |r| r.split(' ').map(&:to_i) }

    @matrix = MyMatrix[*arr]
  end

  def rows
    ->(n) { @matrix.row(n).to_a }
  end

  def columns
    tmp_proc = ->(n) { @matrix.column(n).to_a }

    def tmp_proc.last
      self.call(-1)
    end

    tmp_proc
  end

  public_class_method :new
end
