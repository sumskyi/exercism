# frozen_string_literal: true

module SimpleCalculator
  ALLOWED_OPERATIONS = ['+', '/', '*'].freeze
  UnsupportedOperation = Class.new(StandardError)

  def self.calculate(first_operand, second_operand, operation)
    raise ArgumentError if [first_operand, second_operand].any? { |o| !o.is_a? Integer }

    raise UnsupportedOperation unless ALLOWED_OPERATIONS.include?(operation)

    res = first_operand.public_send(operation.to_sym, second_operand)

    "#{first_operand} #{operation} #{second_operand} = #{res}"
  rescue ZeroDivisionError
    'Division by zero is not allowed.'
  end
end
