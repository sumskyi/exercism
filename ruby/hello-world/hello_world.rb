# frozen_string_literal: true

# hi there
class HelloWorld
  def self.hello(name = 'World')
    "Hello, #{name}!"
  end
end
