# frozen_string_literal: true

class LogLineParser
  attr_reader :log_level, :message

  def initialize(line)
    words = line.scan(/\w+/)
    @log_level = words[0].downcase
    @message = words[1..-1].join(' ')
  end

  def reformat
    "#{@message} (#{@log_level})"
  end
end
